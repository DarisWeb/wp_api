<?php
/*
 * Add my new menu to the Admin Control Panel
 */
 
// Hook the 'admin_menu' action hook, run the function named 'mfp_Add_My_Admin_Link()'
add_action( 'admin_menu', 'mfp_Add_My_Admin_Link' );
 
// Add a new top level menu link to the ACP
function mfp_Add_My_Admin_Link()
{
      add_menu_page(
        'My First Page', // Title of the page
        'Mon plugin', // Text to show on the menu link
        'manage_options', // Capability requirement to see the link
        'my-first-plugin/includes/mfp-first-acp-page.php' // The 'slug' - file to display when clicking the link
    );
}

function prefix_register_post_type()
{
  register_meta('post', 'username', [
    'object_subtype' => 'prefix_portfolio', // Limit to a post type.
    'type'           => 'string',
    'description'    => 'Nom de joueur',
    'single'         => true,
    'show_in_rest'   => true,
]);
register_meta('post', 'date', [
  'object_subtype' => 'prefix_portfolio', // Limit to a post type.
  'type'           => 'string',
  'description'    => 'Date du jeu',
  'single'         => true,
  'show_in_rest'   => true,
]);
register_meta('post', 'score', [
  'object_subtype' => 'prefix_portfolio', // Limit to a post type.
  'type'           => 'string',
  'description'    => 'Score',
  'single'         => true,
  'show_in_rest'   => true,
]);

  register_post_type(
    'prefix_portfolio',
    array(
      'labels'        => array(
        'name'               => __('Portfolio', 'text_domain'),
        'singular_name'      => __('Portfolio', 'text_domain'),
        'menu_name'          => __('Portfolio', 'text_domain'),
        'name_admin_bar'     => __('Portfolio Item', 'text_domain'),
        'all_items'          => __('All Items', 'text_domain'),
        'add_new'            => _x('Add New', 'prefix_portfolio', 'text_domain'),
        'add_new_item'       => __('Add New Item', 'text_domain'),
        'edit_item'          => __('Edit Item', 'text_domain'),
        'new_item'           => __('New Item', 'text_domain'),
        'view_item'          => __('View Item', 'text_domain'),
        'search_items'       => __('Search Items', 'text_domain'),
        'not_found'          => __('No items found.', 'text_domain'),
        'not_found_in_trash' => __('No items found in Trash.', 'text_domain'),
        'parent_item_colon'  => __('Parent Items:', 'text_domain'),
      ),
      'show_in_rest' => true,
      'public'        => true,
      'menu_position' => 5,
      'supports'      => array(
        'title',
        'editor',
        'custom-fields',
      ),
      'taxonomies'    => array(
        'prefix_portfolio_categories',
      ),
      'has_archive'   => true,
      'rewrite'       => array(
        'slug' => 'portfolio',
      ),
    )
  );
}

add_action('init', 'prefix_register_post_type');


function prefix_register_taxonomy()
{
  register_taxonomy(
    'prefix_portfolio_categories',
    array(
      'prefix_portfolio',
    ),
    array(
      'labels'            => array(
        'name'              => _x('Categories', 'prefix_portfolio', 'text_domain'),
        'singular_name'     => _x('Category', 'prefix_portfolio', 'text_domain'),
        'menu_name'         => __('Categories', 'text_domain'),
        'all_items'         => __('All Categories', 'text_domain'),
        'edit_item'         => __('Edit Category', 'text_domain'),
        'view_item'         => __('View Category', 'text_domain'),
        'update_item'       => __('Update Category', 'text_domain'),
        'add_new_item'      => __('Add New Category', 'text_domain'),
        'new_item_name'     => __('New Category Name', 'text_domain'),
        'parent_item'       => __('Parent Category', 'text_domain'),
        'parent_item_colon' => __('Parent Category:', 'text_domain'),
        'search_items'      => __('Search Categories', 'text_domain'),
      ),
      'show_admin_column' => true,
      'hierarchical'      => true,
      'rewrite'           => array(
        'slug' => 'portfolio/category',
      ),
    )
  );
}

add_action('init', 'prefix_register_taxonomy', 0);